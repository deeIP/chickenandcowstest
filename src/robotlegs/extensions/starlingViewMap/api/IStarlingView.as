package robotlegs.extensions.starlingViewMap.api {
	public interface IStarlingView {
		function viewAdded(): void;
		function viewRemoved(): void;
	}
}