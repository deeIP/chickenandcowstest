package
{
	
	import starling.display.Sprite
	import starling.events.Event;
	
	import robotlegs.bender.framework.api.IContext;
	import robotlegs.bender.framework.impl.Context;
	import robotlegs.extensions.starlingViewMap.StarlingViewMapExtension;
	import robotlegs.bender.extensions.mediatorMap.MediatorMapExtension;
	import robotlegs.bender.extensions.localEventMap.LocalEventMapExtension;
	import robotlegs.bender.extensions.eventDispatcher.EventDispatcherExtension;
	import utils.extensions.actor.ActorExtension;
	import robotlegs.bender.extensions.signalCommandMap.SignalCommandMapExtension;
	
	import application.config.ApplicationConfig;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class Game extends Sprite {
		
		private var _context:IContext;
		
		public function Game() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point			
			_context = new Context();
			
			_context.install(
			EventDispatcherExtension,
			SignalCommandMapExtension,
			LocalEventMapExtension,
			MediatorMapExtension,
			StarlingViewMapExtension,
			ActorExtension);
			_context.configure(
				ApplicationConfig,
				ChickensAndCows.starlingInstance);
			_context.initialize();
		}
		
	}
	
}