package application.view {
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import application.view.tile.TiledMapView;
	import application.model.ConfigDataProvider;
	import application.view.ui.MenuView;
	
	public class ApplicationMediator extends StarlingMediator {
		
		
		[Inject]
		public var configDataProvider:ConfigDataProvider;
		
		[Inject]
		public var _applicationView:ApplicationView;
		
		override public function initialize():void {
			_applicationView.screenRoot.addChild(new TiledMapView(configDataProvider.dimX, configDataProvider.dimY));
			_applicationView.uiRoot.addChild(new MenuView());
		}
		
	}
}