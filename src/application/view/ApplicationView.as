package application.view {
	import starling.display.Sprite;
	
	import application.view.tile.TileView;
	import application.view.tile.TiledMapView;
	import application.model.ConfigDataProvider;
	
	
	public class ApplicationView extends Sprite {
		
		private var _screenRoot:Sprite;
		public function get screenRoot():Sprite {
			return _screenRoot;
		}
		
		private var _uiRoot:Sprite;
		public function get uiRoot():Sprite {
			return _uiRoot;
		}
		
		public function ApplicationView() {
			super();
			_screenRoot = new Sprite();
			addChild(_screenRoot);
			
			_uiRoot = new Sprite();
			addChild(_uiRoot);
		}
		
	}
}