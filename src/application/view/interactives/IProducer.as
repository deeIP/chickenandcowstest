package application.view.interactives 
{
	
	/**
	 * ...
	 * @author Dee
	 */
	public interface IProducer {
		function produce():void;
	}
	
}