package application.view.interactives 
{
	
	import starling.display.Sprite;
	import starling.display.Image;
	
	import application.view.IGraphicRequestView;
	import application.model.asset.AtlasItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class ProgressBarView extends Sprite implements IGraphicRequestView {
		
		private var _src:String;
		private var _bar:Image;
		private var _barMax:Number;
		private var _shiftX:Number;
		private var _shiftY:Number;
		
		public function ProgressBarView(shiftX:Number, shiftY:Number) {
			super();
			_shiftX = shiftX;
			_shiftY = shiftY;
			_src = "graphics/sprite_sheet";
		}
		
		public function get src():String {
			return _src;
		}
		
		public function value(value:Number, max:Number):void {
			if (_bar) {
				var calculatedWidth:Number = Math.abs(value) / max * _barMax;
				if (calculatedWidth >= 1) {
					_bar.width = calculatedWidth;
				} else {
					_bar.width = 1;
				}
			}
		}
		
		public function setResource(atlasItem:AtlasItem):void {
			_bar = new Image(atlasItem.textureAtlas.getTexture("progress_bar"));
			_bar.x = _shiftX;
			_bar.y = _shiftY;
			addChild(_bar);
			_barMax = _bar.width;
		}
	}

}