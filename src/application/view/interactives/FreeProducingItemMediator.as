package application.view.interactives
{

	import starling.core.Starling;
	import starling.display.DisplayObjectContainer;
	import starling.animation.IAnimatable;
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	import application.view.interactives.ProgressBarView;
	import application.model.behavior.ICycle;
	import application.model.behavior.AdvanceCycle;
	import application.view.beings.IFreeProducingItem;
	import application.model.GameStateProvider;
	import utils.trigger.Trigger;
	import utils.trigger.ITrigger;
	import application.model.behavior.ISuspendedBeingContext;
	import application.model.ConfigDataProvider;

	/**
	 * ...
	 * @author Dee
	 */
	public class FreeProducingItemMediator extends StarlingMediator implements IAnimatable, IProducer, ISellable {
		[Inject]
		public var gameStateProvider:GameStateProvider;

		[Inject]
		public var configDataProvider:ConfigDataProvider;

		[Inject]
		public var freeProducingItem:IFreeProducingItem;

		private var _produceCycle:ICycle;

		private var _produceBar:ProgressBarView;
		private var _consumeBar:ProgressBarView;

		private var _feeded:Boolean = true;

		private var _advanceTime:Number = 0;

		override public function initialize():void {
			super.initialize();
			chargeCycles();

			//TEST
			(freeProducingItem as DisplayObjectContainer).pivotY = (freeProducingItem as DisplayObjectContainer).height - configDataProvider.heightTile;

			_produceBar = new ProgressBarView(30, -20);

			(freeProducingItem as DisplayObjectContainer).addChild(_produceBar);

			(freeProducingItem as ISuspendedBeingContext).actionPerfomableTrigger.triggerValue = false;
			(freeProducingItem as ISuspendedBeingContext).actionPerfomableTrigger.triggerSignal.add(onContextAction);

			Starling.juggler.add(this);
		}

		override public function destroy():void {
			super.destroy();
			_produceCycle.cycleSignal.remove(onProduce);
			Starling.juggler.remove(this);
		}


		private function chargeCycles():void {
			if (_produceCycle) {
				_produceCycle.cycleSignal.removeAll();
			}
			_produceCycle = new AdvanceCycle(freeProducingItem.produceItem.period);
			_produceCycle.cycleSignal.add(onProduce);
			_produceCycle.start();
		}

		private function onContextAction(trigger:ITrigger):void {
			if (!(freeProducingItem as ISuspendedBeingContext).actionPerfomableTrigger.triggerValue) {
				produce();
				chargeCycles();
			}
		}


		public function advanceTime(time:Number):void {
			_advanceTime += time;
			if (int(_advanceTime * 5) >= 1) {
				_advanceTime = 0;
				if (_produceBar) {
					_produceBar.value(_produceCycle.time, freeProducingItem.produceItem.period);
				}
			}
		}

		private function onProduce():void {
			_produceCycle.pause();
			(freeProducingItem as ISuspendedBeingContext).actionPerfomableTrigger.triggerValue = true;
		}

		public function produce():void {
			gameStateProvider.add(freeProducingItem.produceItem.name, freeProducingItem.produceItem.value);
		}

		public function sell(value:int = 1):void {
			gameStateProvider.add(freeProducingItem.priceItem.name, freeProducingItem.priceItem.value);
			(freeProducingItem as DisplayObjectContainer).parent.removeChild(freeProducingItem as DisplayObjectContainer);
		}

	}

}





