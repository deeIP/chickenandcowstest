package application.view.interactives
{
	import application.model.behavior.ISuspendedBeingContext;
	import flash.display.DisplayObject;
	import starling.core.Starling;
	import starling.animation.IAnimatable;
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	import starling.display.DisplayObjectContainer;
	import utils.trigger.ITrigger;

	import application.model.GameStateProvider;
	import application.model.ConfigDataProvider;
	import application.model.behavior.ICycle;
	import application.model.behavior.ReduceCycle;
	import application.model.behavior.AdvanceCycle;

	import application.view.beings.IDemandingProducingItem;
	/**
	 * ...
	 * @author Dee
	 */
	public class DemandingProducingItemMediator extends StarlingMediator implements IAnimatable, IProducer, IConsumer, ISellable {

		[Inject]
		public var gameStateProvider:GameStateProvider;

		[Inject]
		public var configDataProvider:ConfigDataProvider;

		[Inject]
		public var demandingProducingItemView:IDemandingProducingItem;

		private var _consumeCycle:ICycle;
		private var _produceCycle:ICycle;

		private var _produceBar:ProgressBarView;
		private var _consumeBar:ProgressBarView;

		private var _advanceTime:Number = 0;

		override public function initialize():void {
			super.initialize();
			chargeCycles();

			//TEST
			(demandingProducingItemView as DisplayObjectContainer).pivotY = (demandingProducingItemView as DisplayObjectContainer).height - configDataProvider.heightTile;

			_consumeBar = new ProgressBarView(30, -35);
			_produceBar = new ProgressBarView(30, -20);
			(demandingProducingItemView as DisplayObjectContainer).addChild(_consumeBar);
			(demandingProducingItemView as DisplayObjectContainer).addChild(_produceBar);

			(demandingProducingItemView as ISuspendedBeingContext).actionPerfomableTrigger.triggerValue = false;
			(demandingProducingItemView as ISuspendedBeingContext).actionPerfomableTrigger.triggerSignal.add(onContextAction);

			Starling.juggler.add(this);
		}

		private function chargeCycles():void {
			if (_produceCycle) {
				_produceCycle.cycleSignal.removeAll();
			}
			if (_consumeCycle) {
				_consumeCycle.cycleSignal.removeAll();
			}
			_produceCycle = new AdvanceCycle(demandingProducingItemView.produceItem.period);
			_produceCycle.cycleSignal.add(onProduce);
			_consumeCycle = new ReduceCycle(demandingProducingItemView.consumeItem.period);
			_consumeCycle.cycleSignal.add(onConsume);
			_consumeCycle.start();
			_produceCycle.start();
		}

		override public function destroy():void {
			super.destroy();
			_consumeCycle.cycleSignal.remove(onConsume);
			_produceCycle.cycleSignal.remove(onProduce);
			Starling.juggler.remove(this);
		}

		public function advanceTime(time:Number):void {
			_advanceTime += time;
			if (int(_advanceTime * 5) >= 1) {
				_advanceTime = 0;
				if (_consumeBar) {
					_consumeBar.value(_consumeCycle.time, demandingProducingItemView.consumeItem.period);
				}
				if (_produceBar) {
					_produceBar.value(_produceCycle.time, demandingProducingItemView.produceItem.period);
				}
			}

		}

		private function onContextAction(trigger:ITrigger):void {
			if (!(demandingProducingItemView as ISuspendedBeingContext).actionPerfomableTrigger.triggerValue) {
				consume();
				chargeCycles();
			}
		}

		private function onConsume():void {
			if (Math.round(_produceCycle.time) == demandingProducingItemView.produceItem.period) {
				produce();
			}
			_produceCycle.pause();
			_consumeCycle.pause();
			(demandingProducingItemView as ISuspendedBeingContext).actionPerfomableTrigger.triggerValue = true;
		}

		private function onProduce():void {
			produce();
		}

		public function produce():void {
			gameStateProvider.add(demandingProducingItemView.produceItem.name, demandingProducingItemView.produceItem.value);
		}

		public function consume():void {
			gameStateProvider.take(demandingProducingItemView.consumeItem.name, demandingProducingItemView.consumeItem.value);
		}

		public function sell(value:int = 1):void {
			gameStateProvider.add(demandingProducingItemView.priceItem.name, demandingProducingItemView.priceItem.value);
			(demandingProducingItemView as DisplayObjectContainer).parent.removeChild(demandingProducingItemView as DisplayObjectContainer);
		}
	}

}