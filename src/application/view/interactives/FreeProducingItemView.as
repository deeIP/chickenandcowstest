package application.view.interactives 
{
	import flash.utils.Dictionary;
	import starling.display.Sprite;
	import starling.display.Image;
	import application.view.IGraphicRequestView;
	import application.view.beings.IFreeProducingItem;
	import application.model.behavior.ISuspendedBeingContext;
	import application.model.object.ResourceItem;
	import application.model.asset.AtlasItem;
	import utils.trigger.Trigger;
	
	
	/**
	 * ...
	 * @author Dee
	 */
	public class FreeProducingItemView extends Sprite implements IGraphicRequestView, IFreeProducingItem, ISuspendedBeingContext {
		
		private var _src:String;
		private var _type:String;
		
		private var _consumeItem:ResourceItem;
		private var _produceItem:ResourceItem;
		private var _priceItem:ResourceItem;
		
		private var _actionPerfomableTrigger:Trigger;
		
		public function FreeProducingItemView(dict:Dictionary) {
			_src = "graphics/sprite_sheet";
			//_consumeItem = dict.consumeItem;
			_produceItem = dict.produceItem;
			_priceItem = dict.priceItem;
			_type = dict.type;
			_actionPerfomableTrigger = new Trigger();
		}
		
		public function get src():String {
			return _src;
		}
		
		public function get produceItem():ResourceItem {
			return _produceItem;
		}
		
		public function get priceItem():ResourceItem {
			return _priceItem;
		}
		
		public function setResource(atlasItem:AtlasItem):void {
			var being:Image = new Image(atlasItem.textureAtlas.getTexture(_type));
			addChild(being);
		}
		
		public function performContextAction():void {
			if (_actionPerfomableTrigger.triggerValue) {
				_actionPerfomableTrigger.triggerValue = false;
			}
		}
		
		public function get actionPerfomableTrigger():Trigger {
			return _actionPerfomableTrigger;
		}
		
	}

}