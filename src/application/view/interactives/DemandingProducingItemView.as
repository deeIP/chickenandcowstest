package application.view.interactives
{
	import flash.utils.Dictionary;
	import org.osflash.signals.Signal;
	import starling.display.Sprite;
	import starling.display.Image;
	import utils.trigger.Trigger;
	import application.view.IGraphicRequestView;
	import application.model.asset.AtlasItem;
	import application.model.object.ResourceItem;
	import application.view.beings.IDemandingProducingItem;
	import application.model.behavior.ISuspendedBeingContext;
	/**
	 * ...
	 * @author Dee
	 */
	public class DemandingProducingItemView extends Sprite implements IGraphicRequestView, IDemandingProducingItem, ISuspendedBeingContext {


		//TODO Подумать ещё, возможно наследоваться от одной сущности (это и FreeProducing..) или лучше придумать какой-то агрегатор признаков и поведений объекта
		private var _src:String;
		private var _type:String;

		private var _consumeItem:ResourceItem;
		private var _produceItem:ResourceItem;
		private var _priceItem:ResourceItem;

		private var _actionPerfomableTrigger:Trigger;

		public function DemandingProducingItemView(dict:Dictionary) {
			_src = "graphics/sprite_sheet";
			_consumeItem = dict.consumeItem;
			_produceItem = dict.produceItem;
			_priceItem = dict.priceItem;
			_type = dict.type;
			_actionPerfomableTrigger = new Trigger();
		}

		public function get src():String {
			return _src;
		}

		public function get consumeItem():ResourceItem {
			return _consumeItem;
		}

		public function get produceItem():ResourceItem {
			return _produceItem;
		}

		public function get priceItem():ResourceItem {
			return _priceItem;
		}

		public function setResource(atlasItem:AtlasItem):void {
			var being:Image = new Image(atlasItem.textureAtlas.getTexture(_type));
			addChild(being);
		}

		public function performContextAction():void {
			if (_actionPerfomableTrigger.triggerValue) {
				_actionPerfomableTrigger.triggerValue = false;
			}
		}

		public function get actionPerfomableTrigger():Trigger {
			return _actionPerfomableTrigger;
		}
	}

}