package application.view.interactives 
{
	
	/**
	 * ...
	 * @author Dee
	 */
	public interface ISellable {
		function sell(value:int = 1):void;
	}
	
}