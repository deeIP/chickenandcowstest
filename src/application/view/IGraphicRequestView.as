package application.view 
{
	import application.model.asset.AtlasItem;
	/**
	 * ...
	 * @author Dee
	 */
	public interface IGraphicRequestView {
		function setResource(resource:AtlasItem):void;
		function get src():String;
	}
	
}