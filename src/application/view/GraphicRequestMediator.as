package application.view 
{
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import application.model.ConfigDataProvider;
	import application.model.asset.AtlasItem;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class GraphicRequestMediator extends StarlingMediator {
		
		[Inject]
		public var graphicRequestView:IGraphicRequestView;
		
		[Inject]
		public var configDataProvider:ConfigDataProvider;
		
		override public function initialize():void {
			super.initialize();
			configDataProvider.getAtlasItem(graphicRequestView.src, sourceCallback);
		}
		
		private function sourceCallback(atlasItem:AtlasItem):void {
			graphicRequestView.setResource(atlasItem);
		}
		
	}

}