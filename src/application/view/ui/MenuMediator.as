package application.view.ui
{
	import flash.utils.Dictionary;
	import starling.text.TextField;
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	import application.notifications.NtfTextFieldsRequested;
	import application.notifications.NtfTransactionRequested;
	import application.model.GameStateProvider;


	/**
	 * ...
	 * @author Dee
	 */
	public class MenuMediator extends StarlingMediator
	{
		[Inject]
		public var ntfTextFieldsRequested:NtfTextFieldsRequested;

		[Inject]
		public var ntfTransactionRequested:NtfTransactionRequested;

		[Inject]
		public var gameStateProvider:GameStateProvider;

		[Inject]
		public var menuView:MenuView;

		private var _labels:Vector.<TextField> = new Vector.<TextField>();
		private var _values:Vector.<TextField> = new Vector.<TextField>();
		private var _valuesDict:Dictionary = new Dictionary();

		private var _sellEggs:ButtonView;
		private var _sellMilk:ButtonView;


		override public function initialize():void {
			super.initialize();

			//TODO Базовая инфо панель, заготовка
			for (var i:int = 0; i < 4; i++) {
				var textField:TextField = new TextField(100, 40, '');
				textField.y = (i + 1) * 20;
				_labels.push(textField);
				menuView.addChild(textField);
			}

			_labels[0].text = 'Money:';
			_labels[1].text = 'Wheats:';
			_labels[2].text = 'Eggs:';
			_labels[3].text = 'Milk:';

			for (i = 0; i < 4; i++) {
				var textFieldValue:TextField = new TextField(100, 40, '');
				textFieldValue.x = 100;
				textFieldValue.y = (i + 1) * 20;
				_values.push(textFieldValue);
				menuView.addChild(textFieldValue);
			}
			_valuesDict.money = _values[0];
			_valuesDict.wheats = _values[1];
			_valuesDict.eggs = _values[2];
			_valuesDict.milk = _values[3];

			_sellEggs = new ButtonView("sell_btn");
			_sellEggs.pushSignal.add(onSellEggs);
			menuView.addChild(_sellEggs);
			_sellEggs.x = 170;
			_sellEggs.y = 60;

			_sellMilk = new ButtonView("sell_btn");
			menuView.addChild(_sellMilk);
			_sellMilk.pushSignal.add(onSellMilk);
			_sellMilk.x = 170;
			_sellMilk.y = 95;

			ntfTextFieldsRequested.dispatch(_valuesDict);
		}


		private function onSellEggs():void {
			ntfTransactionRequested.dispatch(gameStateProvider.transaction("sellEggs"));
		}

		private function onSellMilk():void {
			ntfTransactionRequested.dispatch(gameStateProvider.transaction("sellMilk"));
		}

		override public function destroy():void {
			super.destroy();
			_sellEggs.pushSignal.remove(onSellEggs);
			_sellMilk.pushSignal.remove(onSellMilk);
		}

	}

}