package application.view.ui
{
	import starling.display.Sprite;
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import org.osflash.signals.Signal;
	import application.view.IGraphicRequestView;
	import application.model.asset.AtlasItem;
	/**
	 * ...
	 * @author Dee
	 */
	public class ButtonView extends Sprite implements IGraphicRequestView {

		private var _src:String;
		private var _item_src:String;
		private var _pushSignal:Signal = new Signal();

		public function ButtonView(item_src:String) {
			_src = "graphics/sprite_sheet";
			_item_src = item_src;
		}

		public function get src():String {
			return _src;
		}

		public function setResource(atlasItem:AtlasItem):void {
			var button:Image = new Image(atlasItem.textureAtlas.getTexture(_item_src));
			addChild(button);
			addEventListener(TouchEvent.TOUCH, touchHandler);
		}

		public function get pushSignal():Signal {
			return _pushSignal;
		}

		private function touchHandler(e:TouchEvent):void {
			var touch:Touch = e.getTouch(this);
			if (touch && touch.phase == TouchPhase.BEGAN) {
				_pushSignal.dispatch();
			}
		}

	}

}