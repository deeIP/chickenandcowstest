package application.view.beings 
{
	import application.model.object.ResourceItem;
	import org.osflash.signals.Signal;
	/**
	 * ...
	 * @author Dee
	 */
	public interface IDemandingProducingItem {
		function get consumeItem():ResourceItem;
		function get produceItem():ResourceItem;
		function get priceItem():ResourceItem;
	}
	
}