package application.view.beings 
{
	import org.osflash.signals.Signal;
	import application.model.object.ResourceItem;
	/**
	 * ...
	 * @author Dee
	 */
	public interface IFreeProducingItem extends IPriceItem {
		function get produceItem():ResourceItem;
	}
	
}