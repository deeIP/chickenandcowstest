package application.view.tile 
{
	import application.view.interactives.DemandingProducingItemView;
	import starling.display.Sprite;
	import starling.display.Image;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	import application.model.asset.AtlasItem;
	import application.view.IGraphicRequestView;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class TileView extends Sprite implements IGraphicRequestView {
		
		private var _src:String;
		
		public function TileView() {
			super();
			_src = "graphics/sprite_sheet_tile";
			
			
		}
		
		public function get src():String {
			return _src;
		}
		
		private var _atlasItem:AtlasItem;
		public function get atlasItem():AtlasItem {
			return _atlasItem;
		}
		
		public function setResource(atlasItem:AtlasItem):void {
			_atlasItem = atlasItem;
			var tile:Image = new Image(atlasItem.textureAtlas.getTexture("standard_tile"));
			/*tile.pivotX = tile.width / 2;
			tile.pivotY = tile.height / 2;*/
			addChild(tile);
			
		}
		
	}

}