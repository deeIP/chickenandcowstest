package application.view.tile 
{
	
	import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
	
	import application.model.ConfigDataProvider;
	import application.model.object.ResourceItem;
	import application.view.interactives.DemandingProducingItemView;
	import application.model.GameStateProvider;
	/**
	 * ...
	 * @author Dee
	 */
	public class TiledMapMediator extends StarlingMediator {
		
		[Inject]
		public var tiledMapView:TiledMapView;
		
		[Inject]
		public var configDataProvider:ConfigDataProvider;
		
		[Inject]
		public var gameStateProvider:GameStateProvider
		
		override public function initialize():void {
			super.initialize();
			tiledMapView.tileClicked.add(onTileClicked);
		}
		
		private function onTileClicked(i:int, j:int, tileView:TileView):void {
			
			if (configDataProvider.beingsArray[i][j] == null) {
				configDataProvider.beingsArray[i][j] = gameStateProvider.generateRandomAsset();
				tileView.addChild(configDataProvider.beingsArray[i][j]);
			} else {
				configDataProvider.beingsArray[i][j].performContextAction();
			}
		}
		
		override public function destroy():void {
			super.destroy();
			tiledMapView.tileClicked.remove(onTileClicked);
		}
	}

}