package application.view.tile
{

	import flash.geom.Point;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;

	import org.osflash.signals.Signal;

	import application.view.interactives.DemandingProducingItemView;

	import application.model.object.ResourceItem;

	/**
	 * ...
	 * @author Dee
	 */
	public class TiledMapView extends Sprite {

		private var _landMassArray:Array;

		private var _dimX:int;
		private var _dimY:int;


		//TODO Брать из конфига
		private var _baseX:int = 530;
		private var _baseY:int = 20;
		private var _tileHeight:int = 71;
		private var _halfTileHeight:int = 35;

		private var _tileClicked:Signal = new Signal(int, int, TileView);
		public function get tileClicked():Signal {
			return _tileClicked;
		}

		public function TiledMapView(dimX:int, dimY:int) {
			super();
			_dimX = dimX;
			_dimY = dimY;

			//TODO Возможно расстановку лучше делать в медиаторе, чтобы удобнее обращаться к конфигу за данными (или сделать метод с параметрами)
			//Простая расстановка тайлов (не используются превышения)
			_landMassArray = new Array();
			for (var i:int = 0; i < dimY; i++) {
				var includeArray:Array = new Array();
				for (var j:int = 0; j < dimX; j++) {
					includeArray.push(new TileView());
					addChild(includeArray[j]);

					includeArray[j].x = _baseX + _tileHeight * j - _tileHeight * i;
					includeArray[j].y = _baseY + j * _halfTileHeight + i * _halfTileHeight;
				}
				_landMassArray.push(includeArray);
			}


			addEventListener(TouchEvent.TOUCH, touchHandlerTiles);
		}


		private function touchHandlerTiles(e:TouchEvent):void {
			var touch:Touch = e.getTouch(stage);
			if (touch && touch.phase == TouchPhase.BEGAN) {
				var position:Point = touch.getLocation(stage);

				//TODO Отрефакторить, сделать более наглядно, придумать как это сделать без отдельного атласа тайла
				for (var i:int = _dimY - 1; i >= 0; i--) {
					for (var j:int = _dimX - 1; j >= 0; j--) {
						if (_landMassArray[i][j].atlasItem.bitmap.bitmapData.hitTest(new Point(_landMassArray[i][j].x, _landMassArray[i][j].y), 1, new Point(position.x, position.y)) ) {
							_tileClicked.dispatch(i, j, _landMassArray[i][j]);
							return;
						}
					}

				}
			}
		}


	}

}