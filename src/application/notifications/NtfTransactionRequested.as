package application.notifications
{
	import application.notifications.Ntf;

	import application.model.behavior.ITransaction;
	/**
	 * ...
	 * @author Dee
	 */
	public class NtfTransactionRequested extends Ntf{

		public function NtfTransactionRequested(){
			super(ITransaction);
		}

	}

}