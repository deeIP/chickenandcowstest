package application.command
{
	import application.model.behavior.ITransaction;
	import robotlegs.bender.bundles.mvcs.Command;
	import application.model.GameStateProvider;
	/**
	 * ...
	 * @author Dee
	 */
	public class ResolveTransaction extends Command {
		[Inject]
		public var store:GameStateProvider;

		[Inject]
		public var transaction:ITransaction;

		override public function execute():void {
			transaction.attempt(store);
		}

	}

}