package application.command 
{
	import flash.utils.Dictionary;
	import robotlegs.bender.bundles.mvcs.Command;
	import application.model.GameStateProvider;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class ResponceTextFields extends Command {
		
		[Inject]
		public var gameStateProvider:GameStateProvider;
		[Inject]
		public var textFields:Dictionary;
		
		override public function execute():void {
			gameStateProvider.setTfs(textFields);
		}
	}

}