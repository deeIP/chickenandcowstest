package application.model
{
	import application.model.behavior.ExchangeResourcesTransaction;
	import application.model.behavior.IStore;
	import flash.text.TextField;
	import flash.utils.Dictionary;
	import robotlegs.bender.framework.api.IInjector;
	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;

	import application.model.object.ResourceItem;
	import application.view.interactives.DemandingProducingItemView;
	import application.view.interactives.FreeProducingItemView;
	/**
	 * ...
	 * @author Dee
	 */
	public class GameStateProvider implements IStore {

		[Inject]
		public var injector:IInjector;

		private var _resources:Dictionary = new Dictionary();
		private var _resourcesTfs:Dictionary = new Dictionary();

		private var _money:Number;
		private var _wheats:Number;


		public static const CHICKEN:String = 'chicken';
		public static const COW:String = 'cow';
		public static const WHEAT:String = 'wheat';

		public function GameStateProvider() {

		}

		[PostConstruct]
		public function init():void {
			_resources.money = 10000;
			_resources.wheats = 100;
			_resources.eggs = 0;
			_resources.milk = 0;
		}

		public function setTfs(tfs:Dictionary):void {
			_resourcesTfs = tfs;
			//TODO Переделать по нормальному!!
			_resourcesTfs.money.text = _resources.money;
			_resourcesTfs.wheats.text = _resources.wheats;
			_resourcesTfs.eggs.text = _resources.eggs;
			_resourcesTfs.milk.text = _resources.milk;
		}

		private function updateTf(resource:String):void {
			if (_resourcesTfs) {
				_resourcesTfs[resource].text = _resources[resource];
			}
		}

		public function add(resource:String, value:Number):Boolean {
			//TODO реализовать максимум ресурсов
			_resources[resource] += value;
			updateTf(resource);
			return true;
		}

		public function take(resource:String, value:Number):Boolean {
			if (resources[resource] - value >= 0) {
				_resources[resource] -= value;
				updateTf(resource);
				return true;
			} else {
				return false;
			}
		}


		public function get resources():Dictionary {
			return _resources;
		}

		public function get money():Number {
			return _money;
		}

		public function get wheats():Number {
			return _wheats;
		}

		/////////
		//TODO Asset data Это можно вынести во внешний конфиг (например xml)
		//TODO Убрать представления из модели!!
		public function generateRandomAsset():* {
			var rand:int = Math.round(Math.random() * 30);
			var params:Dictionary = new Dictionary();
			/*if (rand < 10) {
				params.consumeItem = new ResourceItem("wheats", 1, 9);
				params.produceItem = new ResourceItem("eggs", 1, 3);
				params.sellableItem = new ResourceItem("money", 100);
				params.type = "standard_chicken";
				return new DemandingProducingItemView(params);
			} else if (rand < 20) {
				params.consumeItem = new ResourceItem("wheats", 1, 12);
				params.produceItem = new ResourceItem("milk", 1, 4);
				params.sellableItem = new ResourceItem("money", 200);
				params.type = "standard_cow";
				return new DemandingProducingItemView(params);
			} else {
				params.consumeItem = null;
				params.produceItem = new ResourceItem("wheats", 1, 4);
				params.sellableItem = new ResourceItem("money", 50);
				params.type = "standard_wheat";
				return new FreeProducingItemView(params);
			}*/
			if (rand < 10) {
				params.consumeItem = new ResourceItem("wheats", 1, 30);
				params.produceItem = new ResourceItem("eggs", 1, 10);
				params.sellableItem = new ResourceItem("money", 100);
				params.type = "standard_chicken";
				return new DemandingProducingItemView(params);
			} else if (rand < 20) {
				params.consumeItem = new ResourceItem("wheats", 1, 20);
				params.produceItem = new ResourceItem("milk", 1, 20);
				params.sellableItem = new ResourceItem("money", 200);
				params.type = "standard_cow";
				return new DemandingProducingItemView(params);
			} else {
				params.consumeItem = null;
				params.produceItem = new ResourceItem("wheats", 1, 10);
				params.sellableItem = new ResourceItem("money", 50);
				params.type = "standard_wheat";
				return new FreeProducingItemView(params);
			}
		}

		//TODO Это тоже данные, брать извне
		public function transaction(key:String):ExchangeResourcesTransaction {
			var spendResources:Vector.<ResourceItem> = new Vector.<ResourceItem>();
			var buyResources:Vector.<ResourceItem> = new Vector.<ResourceItem>();
			if (key == "sellEggs") {
				spendResources.push(new ResourceItem("eggs", 1));
				buyResources.push(new ResourceItem("money", 40));
				return new ExchangeResourcesTransaction(spendResources, buyResources);
			}
			if (key == "sellMilk") {
				spendResources.push(new ResourceItem("milk", 1));
				buyResources.push(new ResourceItem("money", 160));
				return new ExchangeResourcesTransaction(spendResources, buyResources);
			}
			return null;
		}
	}

}