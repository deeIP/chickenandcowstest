package application.model.behavior
{

	/**
	 * ...
	 * @author Dee
	 */
	public interface ITransaction {
		function attempt(store:IStore):void;
		function onSuccess():void;
		function onFail():void;
	}

}