package application.model.behavior
{
	import application.model.object.ResourceItem;
	/**
	 * ...
	 * @author Dee
	 */
	public class ExchangeResourcesTransaction implements ITransaction {

		private var _spendResources:Vector.<ResourceItem>;
		private var _buyResources:Vector.<ResourceItem>;
		public function ExchangeResourcesTransaction(spendResources:Vector.<ResourceItem>, buyResources:Vector.<ResourceItem>) {
			_spendResources = spendResources;
			_buyResources = buyResources;
		}

		public function attempt(store:IStore):void {
			//TODO Важно!!! Сделать комплексную проверку группы ресурсов (чтобы производился откат стора, если некоторые take прошли, а некоторые - нет), наверное в области IStore!!!
			for each (var spendResource:ResourceItem in _spendResources) {
				if (!store.take(spendResource.name, spendResource.value)) {
					onFail();
					return;
				}
			}
			for each (var buyResource:ResourceItem in _buyResources) {
				store.add(buyResource.name, buyResource.value);
			}
			onSuccess();
		}

		public function onSuccess():void {
			//
		}

		public function onFail():void {
			//
		}

	}

}