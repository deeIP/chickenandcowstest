package application.model.behavior 
{
	import utils.trigger.Trigger;
	
	/**
	 * ...
	 * @author Dee
	 */
	public interface ISuspendedBeingContext {
		function performContextAction():void;
		function get actionPerfomableTrigger():Trigger;
	}
	
}