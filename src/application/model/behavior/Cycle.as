package application.model.behavior 
{
	import application.model.behavior.ICycle;
	import starling.core.Starling;
	import starling.animation.IAnimatable;
	import org.osflash.signals.Signal;
	
	import utils.error.NotImplementsError;
	/**
	 * ...
	 * @author Dee
	 */
	public class Cycle implements IAnimatable, ICycle 
	{
		private var _cycleSignal:Signal;
		private var _period:Number = 0;
		private var _paused:Boolean;
		private var _time:Number = 0;
		
		public function Cycle(period:Number) {
			_cycleSignal = new Signal();
			_period = period;
		}
		
		
		/* INTERFACE application.view.interactives.ICyclable */
		
		public function start():void {
			Starling.juggler.add(this);
		}
		
		public function pause():void {
			_paused = true;
		}
		
		public function resume():void {
			_paused = false;
		}
		
		public function get paused():Boolean {
			return _paused;
		}
		
		public function stop():void {
			_cycleSignal.removeAll();
			Starling.juggler.remove(this);
		}
		
		public function get cycleSignal():Signal {
			return _cycleSignal;
		}
		
		public function get time():Number {
			return _time;
		}
		
		public function advanceTime(time:Number):void {
			throw new NotImplementsError();
		}
		
	}

}