package application.model.behavior 
{
	/**
	 * ...
	 * @author Dee
	 */
	public class AdvanceCycle extends Cycle {
		
		private var _time:Number = 0;
		private var _period:Number = 0;
		
		public function AdvanceCycle(period:Number) {
			super(period);
			_period = period;
		}
		
		override public function advanceTime(time:Number):void {
			if (!paused) {
				_time += time;
			}
			if (_time >= _period) {
				
				cycleSignal.dispatch();
				_time = 0;
			}
		}
		
		override public function get time():Number {
			return _time;
		}
	}

}