package application.model.behavior
{

	/**
	 * ...
	 * @author Dee
	 */
	public interface IStore {
		function add(resource:String, value:Number):Boolean;
		function take(resource:String, value:Number):Boolean;
	}

}