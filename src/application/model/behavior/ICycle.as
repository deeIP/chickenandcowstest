package application.model.behavior 
{
	import org.osflash.signals.Signal;
	/**
	 * ...
	 * @author Dee
	 */
	public interface ICycle {
		function start():void;
		function pause():void;
		function resume():void;
		function stop():void;
		function get time():Number;
		function get paused():Boolean;
		function get cycleSignal():Signal;
	}
	
}