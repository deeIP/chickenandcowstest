package application.model.behavior 
{
	/**
	 * ...
	 * @author Dee
	 */
	public class ReduceCycle extends Cycle {
		
		private var _time:Number = 0;
		private var _period:Number = 0;
		
		public function ReduceCycle(period:Number) {
			super(period);
			_time = period;
		}
		
		override public function advanceTime(time:Number):void {
			if (!paused) {
				_time -= time;
			}
			if (_time < 0) {
				_time = _period;
				cycleSignal.dispatch();
			}
		}
		
		override public function get time():Number {
			return _time;
		}
		
	}

}