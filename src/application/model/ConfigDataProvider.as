package application.model
{
	import flash.utils.Dictionary;

	import application.model.asset.AtlasItem;

	/**
	 * ...
	 * @author Dee
	 */
	public class ConfigDataProvider
	{


		public static const ERROR:String = 'error';


		///////////
		private var _atlases:Dictionary = new Dictionary();



		public function ConfigDataProvider() {
		}

		[PostConstruct]
		public function init():void {
			_beingsArray = new Array();
			for (var i:int = 0; i < dimY; i++) {
				var includeArray:Array = new Array();
				for (var j:int = 0; j < dimX; j++) {
					includeArray.push(null);
				}
				_beingsArray.push(includeArray);
			}
		}

		private var _beingsArray:Array;
		public function get beingsArray():Array {
			return _beingsArray;
		}

		public function get dimX():int {
			return 8;
		}

		public function get dimY():int {
			return 8;
		}

		public function get heightTile():Number {
			return 71;
		}

		public function get halfHeightTile():Number {
			return 35;
		}

		public function getAtlasItem(src:String, callback:Function):void {
			if (_atlases[src]) {
				_atlases[src].addCallback(callback);
			} else {
				_atlases[src] = new AtlasItem(src, callback);
			}
		}
	}

}