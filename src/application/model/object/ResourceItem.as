package application.model.object 
{
	/**
	 * ...
	 * @author Dee
	 */
	public class ResourceItem {
		
		private var _name:String;
		private var _value:int;
		private var _period:Number;
		
		public function ResourceItem(name:String, value:int, period:Number = 0) {
			_name = name;
			_value = value;
			_period = period;
		}
		
		public function get name():String {
			return _name;
		}
		
		public function get value():int {
			return _value;
		}
		
		public function get period():Number {
			return _period;
		}
		
	}

}