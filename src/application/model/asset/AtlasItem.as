package application.model.asset 
{
	import flash.display.Bitmap;
	
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	import utils.trigger.ITrigger;
	import utils.trigger.TriggerAndEmitter;
	import utils.trigger.Trigger;
	/**
	 * ...
	 * @author Dee
	 */
	public class AtlasItem {
		private var _bitmap:Bitmap;
		private var _xml:XML;
		private var _textureAtlas:TextureAtlas;
		private var _triggerOnAllLoaded:TriggerAndEmitter;
		private var _triggerBitmapLoaded:Trigger;
		private var _triggerXmlLoaded:Trigger;
		
		private var _callbacks:Vector.<Function> = new Vector.<Function>();
		
		public function AtlasItem(src:String, callback:Function) {
			_triggerOnAllLoaded = new TriggerAndEmitter();
			_triggerBitmapLoaded = new Trigger();
			_triggerXmlLoaded = new Trigger();
			_triggerOnAllLoaded.connect(_triggerBitmapLoaded);
			_triggerOnAllLoaded.connect(_triggerXmlLoaded);
			_triggerOnAllLoaded.triggerSignal.addOnce(onAllLoaded);
			addCallback(callback);
			(new ImageLoader()).load(src + ".png", returnDataBitmap);
			(new XMLLoader()).load(src + ".xml", returnDataXml);
			
		}
		
		public function addCallback(callback:Function):void {
			if (_triggerOnAllLoaded.triggerValue) {
				callback(this);
			} else {
				_callbacks.push(callback);
			}
		}
		
		private function returnDataBitmap(bitmap:Bitmap, purpose:String):void {
			_bitmap = bitmap;
			_triggerBitmapLoaded.triggerValue = true;
		}
		
		private function returnDataXml(xml:XML, purpose:String):void {
			_xml = xml;
			_triggerXmlLoaded.triggerValue = true;
		}
		
		private function onAllLoaded(trigger:ITrigger):void {
			var texture:Texture = Texture.fromBitmap(_bitmap);
			_textureAtlas = new TextureAtlas(texture, _xml);
			for each (var callback:Function in _callbacks) {
				callback(this);
			}
		}
		
		public function get textureAtlas():TextureAtlas {
			return _textureAtlas;
		}
		
		public function get bitmap():Bitmap {
			return _bitmap;
		}
	}

}