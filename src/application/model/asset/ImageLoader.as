package application.model.asset {
	
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.display.Loader; 
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	
	import application.model.ConfigDataProvider;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class ImageLoader implements ILoader
	{
		
		private var callback:Function;
		
		private var param:String;
		
		public function ImageLoader() {
			//
		}
		private var _url:String;
		public function load(url:String, callback:Function, param:String = ''):void {
			_url = url;
			this.callback = callback;
			this.param = param;
			var loader:Loader = new Loader();
			var urlRequest:URLRequest = new URLRequest(url);
			loader.load(urlRequest);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onDataLoad);
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onDataIOError);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onDataSecurityError);
		}
		
		private function onDataLoad(e:Event):void {
			var bitmap:Bitmap = e.target.content;
			this.callback(bitmap, param);
		}
		
		private function onDataIOError(event:Event):void {
			trace('onDataIOError');
			this.callback(null, ConfigDataProvider.ERROR);
		}
		
		private function onDataSecurityError(event:Event):void {
			trace('onDataSecurityError');
			this.callback(null, ConfigDataProvider.ERROR);
		}
		
	}
	
}