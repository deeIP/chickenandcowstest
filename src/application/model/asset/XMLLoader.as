package application.model.asset 
{
	
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.net.URLLoader;
	//import flash.display.Loader; 
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	
	import application.model.ConfigDataProvider;
	
	/**
	 * ...
	 * @author Dee
	 */
	public class XMLLoader implements ILoader {
		
		
		private var callback:Function;
		
		private var param:String;
		
		private var _url:String;
		
		public function XMLLoader() {
			//
		}
		
		
		public function load(url:String, callback:Function, param:String = ''):void {
			_url = url;
			this.callback = callback;
			this.param = param;
			
			var request:URLRequest = new URLRequest();
			request.url = url;
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onResult);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onDataIOError);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onDataSecurityError);
			loader.load(request);
		}
		
		private function onResult(event:Event):void {
			try {
				var xml:XML = XML(event.currentTarget.data);
				this.callback(xml, ConfigDataProvider.ERROR);
			} catch (error:Error) {
				trace('error xml');
				this.callback(null, ConfigDataProvider.ERROR);
				//returnData(0, null, ERROR);
				return;
			}
		}
		
		private function onDataIOError(event:Event):void {
			trace('onDataIOError');
			this.callback(null, ConfigDataProvider.ERROR);
		}
		
		private function onDataSecurityError(event:Event):void {
			trace('onDataSecurityError');
			this.callback(null, ConfigDataProvider.ERROR);
		}
	}

}