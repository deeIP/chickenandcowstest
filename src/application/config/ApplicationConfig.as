package application.config
{
	import starling.core.Starling;
	import robotlegs.bender.framework.api.IConfig;
	import robotlegs.bender.framework.api.IInjector;
	import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.bundles.mvcs.Command;

	import application.view.ApplicationView;
	import application.view.ApplicationMediator;

	import application.model.ConfigDataProvider;
	import application.model.GameStateProvider;

	import application.view.IGraphicRequestView;
	import application.view.GraphicRequestMediator;
	import application.view.tile.TiledMapMediator;
	import application.view.tile.TiledMapView;
	import application.view.tile.TileMediator;
	import application.view.tile.TileView;
	import application.view.interactives.DemandingProducingItemMediator;
	import application.view.beings.IDemandingProducingItem;
	import application.view.interactives.FreeProducingItemMediator;
	import application.view.beings.IFreeProducingItem;
	import application.view.ui.MenuView;
	import application.view.ui.MenuMediator;
	import application.notifications.NtfTextFieldsRequested;
	import application.command.ResponceTextFields;
	import application.notifications.NtfTransactionRequested;
	import application.command.ResolveTransaction;


	/**
	 * ...
	 * @author Dee
	 */
	public class ApplicationConfig implements IConfig {

		[Inject]
		public var injector:IInjector;

		[Inject]
		public var starling:Starling;

		[Inject]
		public var commandMap:ISignalCommandMap;

		[Inject]
		public var mediatorMap:IMediatorMap;

		public function ApplicationConfig() {

		}

		public function configure():void {
			injector.map(ConfigDataProvider).asSingleton(true);
			injector.map(GameStateProvider).asSingleton(true);

			commandMap.map(NtfTextFieldsRequested).toCommand(ResponceTextFields);
			commandMap.map(NtfTransactionRequested).toCommand(ResolveTransaction);

			mediatorMap.map(ApplicationView).toMediator(ApplicationMediator);
			mediatorMap.map(IGraphicRequestView).toMediator(GraphicRequestMediator);

			mediatorMap.map(MenuView).toMediator(MenuMediator);

			mediatorMap.map(TiledMapView).toMediator(TiledMapMediator);
			mediatorMap.map(TileView).toMediator(TileMediator);

			mediatorMap.map(IFreeProducingItem).toMediator(FreeProducingItemMediator);
			mediatorMap.map(IDemandingProducingItem).toMediator(DemandingProducingItemMediator);


			var applicationView:ApplicationView = new ApplicationView();
			injector.injectInto(applicationView);
			starling.stage.addChild(applicationView);
			injector.map(ApplicationView).toValue(applicationView);
		}

	}

}