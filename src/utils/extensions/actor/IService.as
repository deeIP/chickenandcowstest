package utils.extensions.actor
{
	
	/** реализуют классы, которые после регистрации будут инжектиться в другие клкассы */
	public interface IService
	{
		function register(): void;
		function remove(): void;
	}
}