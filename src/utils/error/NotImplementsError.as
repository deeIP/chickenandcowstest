package utils.error
{
	import flash.utils.getQualifiedClassName;
	
	import application.model.text.Formatter;

	public class NotImplementsError extends Error
	{
		public function NotImplementsError(owner: * = null, method: String = null)
		{
			super(Formatter.format(
				"Method or propery is not implements\n" +
				"Class: %s\n" +
				"Method: %s\n", 
				getQualifiedClassName(owner),
				method
			));
		}
	}
}