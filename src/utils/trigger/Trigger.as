package utils.trigger
{
	import org.osflash.signals.Signal;
	
	public class Trigger implements ITrigger {
		
		private var _value: Boolean = false;
		private var _triggerSignal: Signal = new Signal(ITrigger);
		private var _bind: ITrigger;
		
		public function Trigger() {
		}
		
		public function set triggerValue(value:Boolean): void {
			if (value == _value) {
				return;
			}
			_value = value;
			_triggerSignal.dispatch(this);
		} 
		
		public function get triggerValue():Boolean {
			return _bind == null ? _value : _bind.triggerValue;
		}
		
		public function get triggerSignal():Signal {
			return _triggerSignal;
		}
		
		public function bind(to: ITrigger): void {
			if (_bind != null) {
				_bind.triggerSignal.remove(bindHandler);				
			}
			_bind = to;
			if (_bind != null) {
				triggerValue = _bind.triggerValue;
				_bind.triggerSignal.add(bindHandler);				
			}
			
		}
		
		private function bindHandler(t: ITrigger): void {
			triggerValue = _bind.triggerValue;
		}
	}
}