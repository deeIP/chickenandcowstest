package utils.trigger {
	import org.osflash.signals.Signal;
	
	/** триггер может менять свое состояние и уведомлять об этом */
	
	public interface ITrigger {
		function get triggerValue(): Boolean;
		
		/** @return Signal(ITrigger) */
		function get triggerSignal(): Signal;
	}
}