package utils.trigger {
	
	public class TriggerOrEmitter extends TriggerEmitter {
		
		public function TriggerOrEmitter(...args) {
			super(args);
		}
		
		override protected function getTriggerValue(): Boolean {
			if (_triggerList.length == 0) {
				return false;
			}
			for each (var t: ITrigger in _triggerList) {
				if (t.triggerValue) {
					return true;
				}
			}
			return false;
		}
	}
}